// burger menu

const openBtn = document.querySelector(".header__btn-open");
console.log(openBtn)
const closeBtn = document.querySelector(".header__btn-close");
console.log(closeBtn)
const burgerMenu = document.querySelector(".header__container-burger-menu");
console.log(burgerMenu)

const openBurgerMenu = () => {
    openBtn.style.display = "none";
    closeBtn.style.display = "flex";
    burgerMenu.style.display = "flex";
    console.log(openBtn)
}


const closeBurgerMenu = () => {
    closeBtn.style.display = "none";
    openBtn.style.display = "flex";
    burgerMenu.style.display = "none";
    console.log(closeBtn)
}


openBtn.addEventListener("click", () => {
    openBurgerMenu()
});

closeBtn.addEventListener("click", () => {
    closeBurgerMenu()
});