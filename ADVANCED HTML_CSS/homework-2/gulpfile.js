import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import concat from "gulp-concat";
import browserSync from "browser-sync";
import cleancss from "gulp-clean-css";
import autoprefixer from "gulp-autoprefixer";
import del from "del";
import terser from "gulp-terser";
import imagemin from "gulp-imagemin";
import purgecss from "gulp-purgecss";

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const transferImages = () => gulp
	.src("./src/images/**/*")
	.pipe(gulp.dest("./dist/images"));

const minifyImages = () => gulp
	.src("./src/images/**/*")
	.pipe(imagemin())
	.pipe(gulp.dest("./dist/images"));

const js = () =>
	gulp
		.src("./src/scripts/*.js")
		.pipe(concat("scripts.min.js"))
		.pipe(terser())
		.pipe(gulp.dest("./dist/scripts"));

const css = () =>
	gulp
		.src("./src/styles/*.scss")
		.pipe(sass())
		.pipe(cleancss())
		.pipe(
			autoprefixer({
				grid: true,
				overrideBrowserslist: ["last 2 versions"],
			})
		)
		.pipe(
			purgecss({
				content: ["./*.html"],
			})
		)
		.pipe(concat("styles.min.css"))
		.pipe(gulp.dest("./dist/css"))
		.pipe(browserSync.stream());

const resetDist = () => del("./dist");

const mainTasks = gulp.parallel(css, js);

export const build = gulp.series(resetDist, mainTasks, minifyImages);

export const dev = gulp.series(build, () => {
	BS.init({
		server: {
			baseDir: "./",
		},
	});

	gulp.watch(
		["./*.html", "./src/styles/**/*", "./src/scripts/**/*"],
		gulp.series(resetDist, mainTasks, transferImages, (done) => {
			BS.reload();
			done();
		})
	);
});
