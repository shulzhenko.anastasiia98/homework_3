// 1. Як можна оголосити змінну у Javascript?  Змінна в Javascript може бути оголошена як const або let. Різниця між ними в тому, що const - це константа, значення якої ніколи не можна буде змінити, а значення let можна змінювати незлічену кількість разів. Але повторно оголосити змінну const або let неможливо (точніше можливо лише в обмеженому просторі, що позначений фігурними дужками). Також можливо оголошення змінної var, але цей спосіб є застарілим і не має достатнього функціоналу для комфортної роботи, тому вважається неправильним.


// 2. У чому різниця між функцією prompt та функцією confirm?  Promt - це спосіб взаємодії з користувачем, має діалогове вікно з полем для введення та кнопками "ok" і "cancel". У випадку, якщо користувач ввів щось у поле та нажав "ok", введене значення відобразиться в консолі. Якщо була натиснута кнопка "cancel"  або "esc", то виведеться значення "null". Confirm також є способом взаємодії з користувачем, має лише дві кнопки "ok" i "cancel", натискання яких відображатиметься в консолі як true i false.


// 3. Що таке неявне перетворення типів? Наведіть один приклад.  Неявне перетворення типів - це автоматиматична зміна типу з одного в інший (Наприклад, зі строки в число, об'єкта в логічне значення та ін.)       Як приклад можна використати цей вираз '12 / "6"'.В даному випадку "6", хоч і є рядком, але перетвориться на число і дасть вірний результат.



// 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let name = "Nastya";

let admin;

admin = name;

console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 8;

let seconds = days * 86400;

console.log(seconds);



// 3.Запитайте у користувача якесь значення і виведіть його в консоль.

let userName = prompt("What is your name?");

console.log(userName);

