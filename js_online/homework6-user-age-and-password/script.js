//Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
//Для того, щоб використовувати спеціалізованні символи для звичного використання в тексті, який ми виводимо в звичайних або подвійних лапках, ми використовуємо знак "\", який дозволяє нам екранувати символ після нього. 

//Які засоби оголошення функцій ви знаєте?
//Є два способи оголошення функцій: "function expression"(оголошення функції через змінну) i "function declaration" (цей спосіб має на увазі об'явлення функції без змінної ).

//Що таке hoisting, як він працює для змінних та функцій?
//Нoisting дослівно переводиться як "підняття", працює він таким чином, що ми можемо викликати функцію навіть до її оголошення і цей код працюватиме, бо такий контекст роботи Java Script. Зі зміними hoisting також добре працює, тобто змінна може бути ініціалізована та використана ще до того, як її оголосили. 

const createNewUser = () => {
    const firstName = prompt("Enter your name");
    const lastName = prompt("Enter your last name");
    const birthday = prompt("Enter your birthday (dd.mm.yyyy)");   

    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogIn () {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge () {
            return  Math.floor((new Date() - Date.parse(`${this.birthday.slice(-4)}${this.birthday.slice(-5,-4)}${this.birthday.slice(3, 5)}${this.birthday.slice(2, 3)}${this.birthday.slice(0, 2)}`)) / (1000 * 60 * 60 * 24 * 365));
        },
        getPassword () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(-4)}`
        }
    }
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
   
}
console.log(createNewUser());


