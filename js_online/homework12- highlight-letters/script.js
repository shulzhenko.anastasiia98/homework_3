
// Чому для роботи з input не рекомендується використовувати клавіатуру? 
// Події клавіатури призначені для роботи з клавіатурою. Їх можна використовувати для перевірки введення <input>, але будуть недоліки. Наприклад, текст може бути вставлений мишкою, за допомогою правого кліка та меню, без жодного натискання клавіші. В такому випадку корректної роботи не буде.

// TASK 

const buttons = document.querySelectorAll(".btn");
 
document.addEventListener("keydown", (event) => {
    if(Boolean(document.querySelector('.active')) === false){
        buttons.forEach((btn) => {
            if(`Key${btn.innerText}` === event.code || btn.innerText === event.code){
                btn.classList.add("active");
            }    
        })
    } else if(Boolean(document.querySelector('.active')) === true){
            document.querySelector(".active").classList.remove("active");
            buttons.forEach((btn) => {
                if(`Key${btn.innerText}` === event.code || btn.innerText === event.code){
                    btn.classList.add("active");
            }
        })
    }
})
