const userForm = document.querySelector(".password-form");
const userInputs = userForm.querySelectorAll("input");

userForm.addEventListener("submit", (event) => {
    event.preventDefault();
    event.target.reset();
})

const password = document.querySelector("#password");
const repeatPassword = document.querySelector("#repeat_password");
const confirmBtn = document.querySelector(".btn");

const checkPassword = () => {
    if(repeatPassword.value !== password.value) {
        document.querySelector(".error").innerText = "It's necessary to introduse the same values";
        password.addEventListener("focus", () => {document.querySelector(".error").innerText = ""});
    } else {
        document.querySelector(".error").innerText = "";
        alert("You are welcome!");
    }
}  
confirmBtn.addEventListener("click", checkPassword);

userInputs.forEach((input) => {
    input.addEventListener("click", (event) => {
        if(input.type === "password"){
            input.removeAttribute("type");
            input.setAttribute("type", "text");
            event.target.parentNode.children[1].innerHTML = `<img src="./img/eye-off.svg" alt="Show password"/>`;
        } else {
            input.removeAttribute('type');
            input.setAttribute('type', 'password');
            event.target.parentNode.children[1].innerHTML = `<img src="./img/eye.svg" alt="Show password"/>`;   
        }
    })
})



